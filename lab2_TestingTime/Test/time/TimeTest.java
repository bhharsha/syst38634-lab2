/**
 * 
 */
package time;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author bhatt
 *
 */
public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:22:10:12");
		assertTrue("The number of milli seconds are out of range.", totalMilliseconds == 12);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:22:10:0C");
		fail("The number of milli seconds are out of range.");
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:22:10:1000");
		fail("The number of milli seconds are out of range.");
	}

	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:22:10:999");
		assertTrue("The number of milli seconds are out of range.", totalMilliseconds == 999);
	}

	// -------------------------------------------------------------------------------------------

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("04:05:06");
		assertTrue("The time provided does not match the result", totalSeconds == 14706);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("04:00:0A");
		fail("The time provided is not valid");
	}

	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds == 59);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("04:04:60");
		fail("The time provided is not valid");
	}

	// ---------------------------------------------------------------------------------------------

}
